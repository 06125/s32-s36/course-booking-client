let token = localStorage.getItem("token");
let userId = JSON.parse(localStorage.getItem("userDetails"))._id;

const nameProfile = document.querySelector("#name");
const emailProfile = document.querySelector("#email");
const mobileNoProfile = document.querySelector("#mobileNo");
const enrolledCoursesProfile = document.querySelector("#enrolledCourses");

fetch(`http://localhost:3000/api/users/details`, {
  method: "GET",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((profile) => {
    let { firstName, lastName, email, mobileNo, enrollments } = profile;
    nameProfile.innerHTML = `${firstName} ${lastName}`;
    emailProfile.innerHTML = `${email}`;
    mobileNoProfile.innerHTML = `${mobileNo}`;

    console.log(enrolledCoursesProfile);

    if (enrollments.length === 0) return;

    /* const courses = enrollments
      .map((course) => `<li>${course.courseId}</li>`)
      .join("");
    console.log(courses);
    enrolledCoursesProfile.innerHTML = `
        <strong>Enrolled Courses</strong>:
        <ul>${courses}<ul>
    `; */

    enrolledCoursesProfile.innerHTML = `Enrolled Courses`;

    enrollments.forEach((course) => {
      let { courseId } = course;

      fetch(`http://localhost:3000/api/courses/${courseId}`, {
        method: "get",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((result) => result.json())
        .then((result) => {
          console.log(course);
          let { name, description } = result;
          let html = `
              <div class="card"">
                <div class="card-body">
                  <h5 class="card-title">${name}</h5>
                  <p class="card-text">${description}</p>
                  <a href="#" class="btn btn-secondary">See Progress</a>
                </div>
              </div>`;

          enrolledCoursesProfile.insertAdjacentHTML("beforeend", html);
        });
    });
  });
