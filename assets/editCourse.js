let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

const editForm = document.querySelector("#editCourse");
let courseName = document.querySelector("#courseName");
let coursePrice = document.querySelector("#coursePrice");
let courseDesc = document.querySelector("#courseDesc");

let token = localStorage.getItem("token");

fetch(`http://localhost:3000/api/courses/${courseId}`, {
  method: "GET",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    let { name, price, description } = result;
    courseName.value = name;
    coursePrice.value = price;
    courseDesc.value = description;
  });

editForm.addEventListener("submit", function (e) {
  e.preventDefault();

  let name = courseName.value;
  let price = coursePrice.value;
  let description = courseDesc.value;

  fetch(`http://localhost:3000/api/courses/${courseId}/edit`, {
    method: "put",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      name: name,
      price: price,
      description: description,
    }),
  })
    .then((result) => result.json())
    .then((result) => {
      if (result) {
        alert("Successfully Updated the Course");
        window.location.replace("./courses.html");
      } else {
        alert("Something went wrong. Try again.");
      }
    });
});
