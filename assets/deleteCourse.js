let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

let token = localStorage.getItem("token");

fetch(`http://localhost:3000/api/courses/${courseId}/delete`, {
  method: "DELETE",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    if (result) {
      window.location.replace("./courses.html");
    } else {
      alert("Something went wrong");
      window.location.replace("./courses.html");
    }
  });
