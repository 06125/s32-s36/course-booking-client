let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

const courseName = document.querySelector("#courseName");
const courseDescription = document.querySelector("#courseDesc");
const coursePrice = document.querySelector("#coursePrice");
const enrollmentContainer = document.querySelector("#enrollmentContainer");

let token = localStorage.getItem("token");

fetch(`http://localhost:3000/api/courses/${courseId}`, {
  method: "GET",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    // SHow course details in the web page
    let { name, price, description, enrollment } = result;
    courseName.innerHTML = name;
    courseDescription.innerHTML = description;
    coursePrice.innerHTML = price;
    enrollmentContainer.innerHTML = `
        <a href="#" id="enrollButton" class="btn btn-primary text-white btn-block selectButton">Enroll Course</a>
    `;

    // To make enroll button work
    let enrollButton = document.querySelector("#enrollButton");

    enrollButton.addEventListener("click", function (e) {
      e.preventDefault();

      fetch("http://localhost:3000/api/users/enroll", {
        method: "post",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          courseId,
        }),
      })
        .then((result) => result.json())
        .then((result) => {
          console.log(result);
          if (result) {
            alert("Enrolled Successfully");
            window.location.replace("./courses.html");
          } else {
            alert("Something went wrong.");
          }
        });
    });
  });
