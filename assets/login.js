const signInForm = document.querySelector("#loginUser");

signInForm.addEventListener("submit", function (e) {
  e.preventDefault();
  const email = document.querySelector("#email").value;
  const password = document.querySelector("#password").value;

  fetch("http://localhost:3000/api/users/login", {
    method: "post",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ email: email, password: password }),
  })
    .then((result) => result.json())
    .then((result) => {
      // save the token to local storage
      localStorage.setItem("token", result.access);

      return fetch("http://localhost:3000/api/users/details", {
        method: "get",
        headers: { Authorization: `Bearer ${result.access}` },
      });
    })
    .then((result) => result.json())
    .then((result) => {
      let { _id, isAdmin } = result;
      let userDetails = {
        _id: _id,
        isAdmin: isAdmin,
      };
      localStorage.setItem("userDetails", JSON.stringify(userDetails));

      if (result.auth === "failed") {
        alert("Wrong Credentials. Check your email and password.");
      } else {
        alert("Login Successfully");
        window.location.replace("./courses.html");
      }
    });
});
