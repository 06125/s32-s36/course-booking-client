// target the form
const registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", function (e) {
  e.preventDefault();
  const firstName = document.querySelector("#firstName").value;
  const lastName = document.querySelector("#lastName").value;
  const mobileNo = document.querySelector("#mobileNo").value;
  const email = document.querySelector("#email").value;
  const password = document.querySelector("#password").value;
  const password2 = document.querySelector("#password2").value;

  if (password === password2 && mobileNo.length === 11) {
    //   fetch(<url>, <options>).then()
    fetch("http://localhost:3000/api/users/check-email", {
      method: "post",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email: email }),
    })
      .then((result) => result.json())
      .then((result) => {
        if (!result) {
          return fetch("http://localhost:3000/api/users/register", {
            method: "post",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              mobileNo: mobileNo,
              email: email,
              password: password,
            }),
          });
        } else {
          alert("Email already registered.");
          return;
        }
      })
      .then((result) => result?.json())
      .then((result) => {
        if (result) {
          alert("Registered successfully!");
          window.location.replace("./login.html");
        }
      });
  }
});
