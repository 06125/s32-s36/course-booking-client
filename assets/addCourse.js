const createCourseForm = document.querySelector("#createCourse");

// Listen to an event
createCourseForm.addEventListener("submit", function (e) {
  e.preventDefault();

  let token = localStorage.getItem("token");

  const name = document.querySelector("#courseName").value;
  const price = document.querySelector("#coursePrice").value;
  const description = document.querySelector("#courseDesc").value;

  fetch("http://localhost:3000/api/courses/add", {
    method: "post",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      name: name,
      price: price,
      description: description,
    }),
  })
    .then((result) => result.json())
    .then((result) => {
      if (result) {
        alert("Course Created Successfully");
        window.location.replace("./courses.html");
      } else {
        alert("Course Creation Failed.");
      }
    });
});
