let token = localStorage.getItem("token");
const adminUser = JSON.parse(localStorage.getItem("userDetails")).isAdmin;

const adminButton = document.querySelector("#adminButton");

if (!adminUser) {
  adminButton.innerHTML = null;
}

fetch(`http://localhost:3000/api/courses/${adminUser ? "all" : "active"}`, {
  method: "get",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    let courseData;
    let cardFooter;

    if (result.length !== 0) {
      courseData = `No Courses Available`;
    } else {
    }
    courseData = result.map((course) => {
      if (!adminUser) {
        cardFooter = `
						<a href="./singleCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block selectButton">
							Select Course
						</a>
					`;
      } else {
        if (course.isActive) {
          cardFooter = `
							<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton">
								Edit Course
							</a>
												
							<a href="./archiveCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block archiveButton" data-id="${course._id}">
								Archive Course
							</a>

              <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-secondary text-white btn-block archiveButton" data-id="${course._id}">
								Delete Course
							</a>
						`;
        } else {
          cardFooter = `
							<a href="./unarchiveCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block unarchiveButton">
								Unarchive Course
							</a>

              <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-secondary text-white btn-block archiveButton" data-id="${course._id}">
								Delete Course
							</a>
						`;
        }
      }

      return `
          <div class="col-md-5 my-5">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">${course.name}</h5>
                <p class="card-text text-left">${course.description}</p>
                <p class="card-text text-right">${course.price}</p>
              </div>
              <div class="card-footer">
                ${cardFooter}
              </div>     
            </div>   
          </div> 
              `;
    });

    console.log(courseData);
    let courseContainer = document.querySelector("#courseContainer");
    courseContainer.innerHTML = courseData;
  });
