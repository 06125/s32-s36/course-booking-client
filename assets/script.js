const navSession = document.querySelector("#navSession");
const register = document.querySelector("#register");
const profile = document.querySelector("#profile");

const userToken = localStorage.getItem("token");
const userDetails = JSON.parse(localStorage.getItem("userDetails"));

if (!userToken) {
  navSession.innerHTML = `
        <li class="nav-item">
            <a href="./login.html" class="nav-link">Login</a>
        </li>`;

  register.innerHTML = `
        <li class="nav-item">
            <a href="./register.html" class="nav-link">Register</a>
        </li>`;
} else {
  navSession.innerHTML = `
        <li class="nav-item">
            <a href="./profile.html" class="nav-link">Profile</a>
        </li>`;
  profile.innerHTML = `
        <li class="nav-item">
            <a href="./logout.html" class="nav-link">Logout</a>
        </li>`;

  //   navSession.innerHTML = `
  //         <li class="nav-item">
  //             <a href="./register.html" class="nav-link">Register</a>
  //         </li>`;
}
